# Generated by Django 3.2.16 on 2022-11-06 16:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('receitas', '0004_remove_receita_dificuldade'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='receita',
            name='tag1',
        ),
        migrations.RemoveField(
            model_name='receita',
            name='tag2',
        ),
    ]
