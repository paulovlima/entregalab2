from django.contrib import admin

# Register your models here.
from django.contrib import admin

from .models import Receita, Comment, Category

admin.site.register(Receita)
admin.site.register(Comment)
admin.site.register(Category)