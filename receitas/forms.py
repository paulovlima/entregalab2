from django.forms import ModelForm
from .models import Comment


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
            'date'
        ]
    labels = {
        'author': "Usuário",
        'text': 'Comentário',
        'date': 'Data',
    }