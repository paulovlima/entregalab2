movie_data = [{
    "id":
    "1",
    "name":
    "Mandioca Frita",
    "desc":
    "Mandioca Frita crocante",
    "image":
    "https://cdn.panelinha.com.br/receita/964494000000-Mandioca-frita.jpg",
    "dificuldade":
    "Fácil",
    'ingredientes':
    '250g de mandioca.\nÓleo suficiente pra frita-las.\n 6 dentes de alho grandes espremidos.\n Sal a gosto.',
    'tempo':
    '50 minutos',
    'modo_preparo':
    '''Lave as mandiocas.
Deixe-as de molho em água para facil.
Descasque-as e corte em pedaços de aproximidamente 7cm de comprimento.
Cozinhe os pedaços na panela de pressão, por 30 minutos, de modo a deixa-los al dente.
Após o cozimento, corte-as, ainda quentes, tipo palito e frite-as em óleo quente até dourar.
Retire-as e coloques-as em papel absorvente e reserve.
Unte uma outra panela com um pouco de óleo que usou para fritar a mandioca.
Frite o alho espremido até dourá-lo.
junte aí as mandiocas já fritas.
Salpique sal ou fondor e sirva.'''

}, {
    "id":
    "2",
    "name":
    "Arroz Branco",
    "dificuldade":
    "Fácil",
    "image":
    "https://cdn5.soreceitasfaceis.com/wp-content/uploads/2015/08/arroz-branco-soltinho.jpg",
    "tempo":"30 minutos",
    "desc":"Arroz branco soltinho",
    "ingredientes": "2 dentes de alho picado\n1 fio de óleo\n2 xícaras de arroz\n3 xícaras de água quente",
    'modo_preparo': '''Lave bem o arroz.
Coloque a água para ferver.
Soque  alho picado.
Em seguida, numa panela, coloque o alho picado e  espere.
Acrescente então o óleo e deixe dourar.
Quando o alho começar a suar, adicione o arroz.
Refogue o arroz e, em seguida, coloque a água quente e o sal.
Baixe o fogo, deixe a panela semi-tampada e deixe cozinhar até a água secar.
Pronto.'''

}, {
    "id":
    "3",
    "name":
    "Bolo de Cenoura",
    'dificuldade':'Fácil',
    'tempo': '40 minutos',
    'desc':'Bolo de cenoura para café da tarde',
    "image":
    "https://img.itdg.com.br/tdg/images/recipes/000/000/023/323619/323619_original.jpg?mode=crop&width=710&height=400",
    'ingredientes':'1/2 xícara (chá) de açúcar\n 3 cenouras médias raladas\n 4 ovos\n 2 xícaras de farinha de triho\n 3/2 xícaras de farinha de trigo\n 1 colher de sopa de fermento em pó',
    'modo_preparo': 'Em um liquidificador, adicione a cenoura, os ovos e o óleo, depois misture.\n Acrescente o açúcar e bata novamente por 5 minutos.\n Em uma tigela ou na batedeira, adicione a farinha de trigo e depois misture novamente.\n Acrescente o fermento e misture lentamente com uma colher.\n Asse em um forno preaquecido a 180 graus pro aproximadamente 40 minutos'
}]