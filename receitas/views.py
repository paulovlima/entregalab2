from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Receita, Comment, Category
from django.shortcuts import render, get_object_or_404
from .forms import CommentForm
from django.views import generic


def detail_receita(request, receita_id):
    receita = get_object_or_404(Receita,pk = receita_id)
    context = {'receitas':receita}
    return render(request, 'receitas/detail.html',context)

def search_receita(request):
    context = {}
    if request.GET.get('query',False):
        search_term = request.GET['query'].lower()
        receita_list = Receita.objects.filter(name__icontains = search_term)
        context = {"receita_list": receita_list}
    return render(request, 'receitas/search.html', context)

def create_receita(request):
    if request.method == 'POST':
        receita_name = request.POST['name']
        receita_dificuldade = request.POST['dificuldade']
        receita_tempo = request.POST['tempo']
        receita_image = request.POST['image']
        receita_desc = request.POST['desc']
        receita_ingredientes = request.POST['ingredientes']
        receita_prepaor = request.POST['modo_prepaor']
        receita_tag1 = request.POST['tag1']
        receita_tag2 = request.POST['tag2']
        receita = Receita(
            name = receita_name,
            dificuldade = receita_dificuldade,
            tempo = receita_tempo,
            image = receita_image,
            desc = receita_desc,
            ingredientes = receita_ingredientes,
            modo_prepaor = receita_prepaor,
            tag1 = receita_tag1,
            tag2 = receita_tag2
        )
        receita.save()
        return HttpResponseRedirect(
            reverse('receitas:detail', args=(receita.id, )))
    else:
        return render(request, 'receitas/create.html', {})

def update_receita(request, receita_id):
    receita = get_object_or_404(Receita, pk = receita_id)

    if request.method == "POST":
        receita.name = request.POST['name']
        receita.dificuldade = request.POST['dificuldade']
        receita.tempo = request.POST['tempo']
        receita.image = request.POST['image']
        receita.desc = request.POST['desc']
        receita.ingredientes = request.POST['ingredientes']
        receita.modo_prepaor = request.POST['modo_prepaor']
        receita.tag1 = request.POST['tag1']
        receita.tag2 = request.POST['tag2']
        receita.save()
        return HttpResponseRedirect(
            reverse('receitas:detail', args=(receita.id, )))

    context = {'receitas':receita}
    return render(request, 'receitas/update.html', context)

def delete_receita(request,receita_id):
    receita = get_object_or_404(Receita, pk = receita_id)

    if request.method == "POST":
        receita.delete()
        return HttpResponseRedirect(reverse('receitas:index'))
    context = {'receitas':receita}
    return render(request, 'receitas/delete.html',context)

def create_comment(request, receita_id):
    receita = get_object_or_404(Receita,pk = receita_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(
                author = comment_author,
                text = comment_text,
                receita = receita
            )
            comment.save()
            return HttpResponseRedirect(
                reverse('receitas:detail', args=(receita_id, ))
            )
    else:
        form = CommentForm()
    context = {'form':form, 'receitas':receita}
    return render(request, 'receitas/comment.html', context)

def list_category(request, category_id):
    tag = get_object_or_404(Category, pk = category_id)
    context = {'category':tag}
    return render(request, 'receitas/cat_list.html', context)

class ReceitaListView(generic.ListView):
    model = Receita
    template_name = 'receitas/index.html'

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'receitas/category.html'