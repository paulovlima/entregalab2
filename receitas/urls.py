from django.urls import path

from . import views

app_name = 'receitas'

urlpatterns = [
    path('', views.ReceitaListView.as_view(), name='index'),
    path('search/', views.search_receita, name='search'),
    path('<int:receita_id>/', views.detail_receita, name = 'detail'),
    path('create/', views.create_receita, name='create'),
    path('update/<int:receita_id>/', views.update_receita, name='update'),
    path('delete/<int:receita_id>/', views.delete_receita, name='delete'),
    path('<int:receita_id>/comment/', views.create_comment, name='comment'),
    path('category/', views.CategoryListView.as_view(), name = 'category'),
    path('cat_list/<int:category_id>', views.list_category, name = 'cat_list')
]