from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.

class Receita(models.Model):
    name = models.CharField(max_length=250)
    tempo = models.CharField(max_length=250)
    ingredientes = models.TextField()
    modo_prepaor = models.TextField()
    desc = models.CharField(max_length=250)
    image = models.URLField()

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    date = models.DateTimeField(default = timezone.now)
    receita = models.ForeignKey(Receita, on_delete=models.CASCADE)

    def __srt__(self):
        return '{0} - {1}'.format(self.text, self.author.username)

class Category(models.Model):
    name  = models.CharField(max_length = 250)
    description = models.CharField(max_length=250)
    receitas = models.ManyToManyField(Receita)